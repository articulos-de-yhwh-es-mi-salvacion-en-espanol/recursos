#  La Biblia en audio, WordProject

Bendiciones, paz y gozo. El día de hoy compartiremos un recurso que será de mucha bendición para tu vida, se trata de el sitio web llamado **WordProject**.

Este sitio web contiene la Biblia en audio y escrita en varios idiomas, se diferencia de otros sitios por las siguientes razones:

1. Se pueden descargar los audios de los libros de la Biblia.
2. El objetivo es poder dar versiones escritas y en audio en diferentes idiomas.
3. El uso de los recursos es libre porque proviene de las versiones de Domino Público, es decir, que no tenemos solicitar permiso para su uso.

El link del sitio web es el siguiente https://www.wordproject.org

## Descargando audios

Para poder descargar el libro de la Biblia que deseamos debemos hacerlo siguiente:

1. Entramos a https://www.wordproject.org

2. Luego presionamos en **Audio**

![Ir al menu](./menu-audio.png)

3. Buscamos en la página el idioma que queremos

![Seleccionar el idioma](./select-spanish-audio.png)

4. Seleccionamos el libro que queremos descargar

![Seleccionar el libro](select-book-audio.png)

5. Nos mostrará un reproductor para oír el libro en línea

![Libro con los audios](audio-bible-book.png)

6. Al final de la lista de reproducción se mostrará un botón con el nombre de **ZIP**, este es el que nos permitirá descargar el libro. Al presionarlo comenzará a descargar el libro.

![Descargar zip](./download-zip.png)

7. Al finalizar la descarga tendremos un archivo compreso (.zip) con los audios únicamente debemos descomprimirlo y tendremos la carpeta con los archivos de audio del libro de la Biblia.

Bendiciones, esperamos que sea bendición.